import 'package:flutter/material.dart';

const double canvasSize = 370;
const double borderSize = 4;
const double strokeWidth = 16;
const double mnistSize = 250;
final Color? backgroundColor = Colors.grey[200];
