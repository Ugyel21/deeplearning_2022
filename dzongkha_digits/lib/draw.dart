import 'dart:io';
import 'dart:ui';
import 'package:dzongkha_digits/constants.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/rendering.dart';
import 'package:dzongkha_digits/predicted_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tflite/flutter_tflite.dart';
import 'dart:ui' as ui;

class Draw extends StatefulWidget {
  const Draw({super.key});

  @override
  State<Draw> createState() => _DrawState();
}

class _DrawState extends State<Draw> {
  GlobalKey globalKey = GlobalKey();
  final pointMode = ui.PointMode.points;
  File? _image;
  late List _output;
  final _points = <DrawingArea?>[];

  @override
  void initState() {
    super.initState();
    loadModel().then((value) {
      setState(() {});
    });
  }

  Future<void> _save() async {
    final RenderRepaintBoundary boundary =
        globalKey.currentContext!.findRenderObject()! as RenderRepaintBoundary;
    final image = await boundary.toImage();
    final byteData = await image.toByteData(format: ImageByteFormat.png);
    final imageBytes = byteData?.buffer.asUint8List();
    if (imageBytes != null) {
      final directory = await getApplicationDocumentsDirectory();
      final imagePath =
          await File('${directory.path}/container_image.png').create();
      await imagePath.writeAsBytes(imageBytes);
      classifyingImage(imagePath);
    }
  }

  @override
  void dispose() {
    super.dispose();
    Tflite.close();
  }

  classifyingImage(image) async {
    var output = await Tflite.runModelOnImage(
      path: image.path,
      numResults: 11,
      threshold: 0.5,
      imageMean: 0.0,
      imageStd: 200.0,
      asynch: true,
    );
    setState(() {
      _output = output!;

      String _name = '${_output[0]['label']}';
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => PredictedScreen(name: _name)));
    });
  }

  loadModel() async {
    await Tflite.loadModel(
        model: 'assets/handwritten.tflite', labels: 'assets/labels.txt');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 1,
        centerTitle: false,
        title: const Text(
          "Dzongkha Handwritten Digit",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18.0,
            fontFamily: 'Roboto-Thin',
            color: Colors.white,
          ),
        ),
        backgroundColor: const Color.fromARGB(255, 61, 85, 222),
        elevation: 0,
      ),
      body: Align(
        alignment: Alignment.center,
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            const Text("Draw a Dzongkha digit",
                style: TextStyle(
                    fontSize: 20,
                    fontFamily: 'Roboto-Thin',
                    color: Colors.black)),
            const SizedBox(
              height: 20,
            ),
            _drawCanvasWidget(),
            const SizedBox(
              height: 40,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 50, //height of button
                  width: 150,
                  child: TextButton(
                    style: TextButton.styleFrom(
                      primary: Colors.white,
                      backgroundColor: Colors.blue,
                      onSurface: Colors.grey,
                    ),
                    onPressed: () {
                      _save();
                    },
                    child: const Text(
                      'Predict',
                      style: TextStyle(
                          fontSize: 18.0,
                          fontFamily: 'Roboto-Thin',
                          color: Colors.white),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 40,
                ),
                SizedBox(
                  height: 50, //height of button
                  width: 150,
                  child: TextButton(
                    style: TextButton.styleFrom(
                      primary: Colors.white,
                      backgroundColor: Colors.blue,
                      onSurface: Colors.grey,
                    ),
                    onPressed: () {
                      _points.clear();
                      setState(() {});
                    },
                    child: const Text(
                      'Clear',
                      style: TextStyle(
                          fontSize: 18.0,
                          fontFamily: 'Roboto-Thin',
                          color: Colors.white),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget _drawCanvasWidget() {
    return Container(
      width: canvasSize + borderSize * 2,
      height: canvasSize + borderSize * 2,
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.black,
          width: borderSize,
        ),
      ),
      child: RepaintBoundary(
        key: globalKey,
        child: GestureDetector(
          onPanDown: (DragDownDetails details) {
            setState(() {
              _points.add(DrawingArea(
                  point: details.localPosition,
                  areaPaint: Paint()
                    ..strokeCap = StrokeCap.round
                    ..isAntiAlias = true
                    ..color = Colors.white
                    ..strokeWidth = strokeWidth));
            });
          },
          onPanUpdate: (DragUpdateDetails details) {
            setState(() {
              _points.add(DrawingArea(
                  point: details.localPosition,
                  areaPaint: Paint()
                    ..strokeCap = StrokeCap.round
                    ..isAntiAlias = true
                    ..color = Colors.white
                    ..strokeWidth = strokeWidth));
            });
          },
          onPanEnd: (DragEndDetails details) async {
            _points.add(null);
            setState(() {});
          },
          child: CustomPaint(
            painter: DrawingPainter(_points),
          ),
        ),
      ),
    );
  }
}

class DrawingArea {
  Offset point;
  Paint areaPaint;

  DrawingArea({required this.point, required this.areaPaint});
}

class DrawingPainter extends CustomPainter {
  final List<DrawingArea?> points;

  DrawingPainter(this.points);

  @override
  void paint(Canvas canvas, Size size) {
    Paint background = Paint()..color = Colors.black;
    Rect rect = Rect.fromLTWH(0, 0, size.width, size.height);
    canvas.drawRect(rect, background);
    canvas.clipRect(rect);

    for (int x = 0; x < points.length - 1; x++) {
      if (points[x] != null && points[x + 1] != null) {
        canvas.drawLine(
            points[x]!.point, points[x + 1]!.point, points[x]!.areaPaint);
      } else if (points[x] != null && points[x + 1] == null) {
        canvas.drawPoints(
            PointMode.points, [points[x]!.point], points[x]!.areaPaint);
      }
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
