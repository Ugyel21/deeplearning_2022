import 'package:dzongkha_digits/splash_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Classification',
      home: SplashScreen(),
      debugShowCheckedModeBanner: false,
    );
  }
}