import 'package:dzongkha_digits/about_us.dart';
import 'package:dzongkha_digits/choice_screen.dart';
import 'package:dzongkha_digits/profile.dart';
import 'package:flutter/material.dart';

class NavBar extends StatelessWidget {
  const NavBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          UserAccountsDrawerHeader(
            accountName: const Text("Dzongkha Handwritten Digit",
            style: TextStyle(
              fontFamily: 'Roboto-Medium',
              fontSize: 18.0,
              color: Colors.black
            ),),
            accountEmail: const Text("ཨང་གྲང་།",
            style: TextStyle(
              fontFamily: 'Roboto-Thin',
              fontSize: 18.0,
              color: Colors.black
            ),),
            currentAccountPicture: Center(
              child: CircleAvatar(
                child: ClipOval(
                child: Image.asset(
                  "assets/logo.png",
                  width: 150,
                  height: 150,
                  fit: BoxFit.cover,
                ),
              )),
            ),
            decoration: const BoxDecoration(
              color: Color.fromARGB(255,61,85,222),
            ),
          ),
          ListTile(
            leading: const Icon(Icons.image),
            title: const Text("Developers Profile",
            style: TextStyle(
              fontFamily: 'Roboto-Thin',
              fontSize: 18.0,
              color: Colors.black
            ),),
            onTap: () {
              Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => const Profile()));
            },
          ),
          ListTile(
            leading: const Icon(Icons.home_outlined),
            title: const Text("Home",
            style: TextStyle(
              fontFamily: 'Roboto-Thin',
              fontSize: 18.0,
              color: Colors.black
            ),),
            onTap: () {
              Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => const ChoiceScreen()));
            },
          ),
          ListTile(
            leading: const Icon(Icons.help),
            title: const Text("Help",
            style: TextStyle(
              fontFamily: 'Roboto-Thin',
              fontSize: 18.0,
              color: Colors.black
            ),),
            onTap: () {
              Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => const AboutUs()));
            },
          ),
        ],
      ),
    );
  }
}
