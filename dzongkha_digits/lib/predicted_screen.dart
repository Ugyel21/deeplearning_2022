import 'package:flutter/material.dart';
import 'package:audioplayers/audioplayers.dart';

class PredictedScreen extends StatefulWidget {
  final String name;
  const PredictedScreen({super.key, required this.name});

  @override
  State<PredictedScreen> createState() => _PredictedScreenState(name: name);
}

class _PredictedScreenState extends State<PredictedScreen> {
  final String name;
  late String message;
  final player = AudioCache();
  var play;
  _PredictedScreenState({required this.name});

  @override
  void initState() {
    super.initState();
    check();
  }

  void check() {
    Map<String, String> dict = {
      "Unknown": "No such Number in Dzongkha",
      "༠": "ལེ་ཀོར།",
      "༡": "གཅིག།",
      "༢": "གཉིས།",
      "༣": "གསུམ།",
      "༤": "བཞི།",
      "༥": "ལྔ་།",
      "༦": "དྲུག།",
      "༧": "བདུན།",
      "༨": "བརྒྱད།",
      "༩": "དགུ།"
    };
    String? key = dict[name.trim()];
    message = "$key";
    if (name == "༠") {
      player.play("༠.mp3");
      play = "༠.mp3";
    }
    if (name == "༡") {
      player.play("༡.mp3");
      play = "༡.mp3";
    }
    if (name == "༢") {
      player.play("༢.mp3");
      play = "༢.mp3";
    }
    if (name == "༣") {
      player.play("༣.mp3");
      play = "༣.mp3";
    }
    if (name == "༤") {
      player.play("༤.mp3");
      play = "༤.mp3";
    }
    if (name == "༥") {
      player.play("༥.mp3");
      play = "༥.mp3";
    }
    if (name == "༦") {
      player.play("༦.mp3");
      play = "༦.mp3";
    }
    if (name == "༧") {
      player.play("༧.mp3");
      play = "༧.mp3";
    }
    if (name == "༨") {
      player.play("༨.mp3");
      play = "༨.mp3";
    }
    if (name == "༩") {
      player.play("༩.mp3");
      play = "༩.mp3";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 1,
        centerTitle: false,
        title: const Text(
          "Dzongkha Handwritten Digit",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18.0,
            fontFamily: 'Roboto-Thin',
            color: Colors.white,
          ),
        ),
        backgroundColor: const Color.fromARGB(255, 61, 85, 222),
        elevation: 0,
      ),
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center(
              child: SizedBox(
                child: Text(
                  name,
                  style: const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Roboto-Medium',
                    fontSize: 60.0,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            Center(
              child: Text(
                message,
                style: const TextStyle(
                  color: Colors.black,
                  fontSize: 20.0,
                  fontFamily: 'Roboto-Thin',
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Center(
              child: ElevatedButton(
                child: const Text("Play"),
                onPressed: () {
                  player.play(play);
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
