import 'package:dzongkha_digits/nav_bar.dart';
import 'package:flutter/material.dart';

class Profile extends StatelessWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const NavBar(),
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 61, 85, 222),
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: 230,
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(50),
                ),
                color: Color.fromARGB(255, 61, 85, 222),
              ),
              child: Stack(
                children: [
                  Positioned(
                      top: 20,
                      left: 0,
                      child: Container(
                        height: 100,
                        width: 300,
                        decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(50),
                                bottomRight: Radius.circular(50))),
                      )),
                  const Positioned(
                      top: 50,
                      left: 20,
                      child: Text(
                        "Team Profile",
                        style: TextStyle(
                          fontSize: 20,
                          fontFamily: 'Roboto-Thin',
                          color: Colors.black,
                        ),
                      ))
                ],
              ),
            ),
            SizedBox(
              height: 230,
              child: Stack(
                children: [
                  Positioned(
                      top: 0,
                      left: 10,
                      right: 10,
                      child: Material(
                        child: Container(
                          height: 180,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(0.0),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.3),
                                spreadRadius: 4.0,
                                blurRadius: 20.0,
                                offset: const Offset(
                                    -10.0, 10.0), // changes position of shadow
                              ),
                            ],
                          ),
                        ),
                      )),
                  Positioned(
                      top: 15,
                      left: 30,
                      child: Card(
                        elevation: 10.0,
                        shadowColor: Colors.grey.withOpacity(0.5),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: Container(
                          height: 150,
                          width: 150,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              image: const DecorationImage(
                                fit: BoxFit.fill,
                                image: AssetImage("assets/penjor.jpeg"),
                              )),
                        ),
                      )),
                  Positioned(
                      top: 50,
                      left: 200,
                      child: SizedBox(
                        height: 200,
                        width: 600,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: const [
                            Text(
                              "Team Leader",
                              style: TextStyle(
                                  fontSize: 20,
                                  fontFamily: 'Roboto-Thin',
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            ),
                            Divider(
                              color: Colors.black,
                            ),
                            Text(
                              "Mr. Tshering Penjor",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontFamily: 'Roboto-Thin',
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              "Student",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontFamily: 'Roboto-Thin',
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              "GCIT",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontFamily: 'Roboto-Thin',
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ))
                ],
              ),
            ),
            SizedBox(
              height: 230,
              child: Stack(
                children: [
                  Positioned(
                      top: 0,
                      left: 10,
                      right: 10,
                      child: Material(
                        child: Container(
                          height: 180,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(0.0),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.3),
                                spreadRadius: 4.0,
                                blurRadius: 20.0,
                                offset: const Offset(
                                    -10.0, 10.0), // changes position of shadow
                              ),
                            ],
                          ),
                        ),
                      )),
                  Positioned(
                      top: 15,
                      left: 30,
                      child: Card(
                        elevation: 10.0,
                        shadowColor: Colors.grey.withOpacity(0.5),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: Container(
                          height: 150,
                          width: 150,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              image: const DecorationImage(
                                fit: BoxFit.fill,
                                image: AssetImage("assets/DG.jpeg"),
                              )),
                        ),
                      )),
                  Positioned(
                      top: 50,
                      left: 200,
                      child: SizedBox(
                        height: 200,
                        width: 600,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: const [
                            Text(
                              "Lead Programmer",
                              style: TextStyle(
                                  fontSize: 20,
                                  fontFamily: 'Roboto-Thin',
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            ),
                            Divider(
                              color: Colors.black,
                            ),
                            Text(
                              "Mr. Dorji Gyeltshen",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontFamily: 'Roboto-Thin',
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              "Student",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontFamily: 'Roboto-Thin',
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              "GCIT",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontFamily: 'Roboto-Thin',
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ))
                ],
              ),
            ),
            SizedBox(
              height: 230,
              child: Stack(
                children: [
                  Positioned(
                      top: 0,
                      left: 10,
                      right: 10,
                      child: Material(
                        child: Container(
                          height: 180,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(0.0),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.3),
                                spreadRadius: 4.0,
                                blurRadius: 20.0,
                                offset: const Offset(
                                    -10.0, 10.0), // changes position of shadow
                              ),
                            ],
                          ),
                        ),
                      )),
                  Positioned(
                      top: 15,
                      left: 30,
                      child: Card(
                        elevation: 10.0,
                        shadowColor: Colors.grey.withOpacity(0.5),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: Container(
                          height: 150,
                          width: 150,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              image: const DecorationImage(
                                fit: BoxFit.fill,
                                image: AssetImage("assets/tenzinkelzang.jpeg"),
                              )),
                        ),
                      )),
                  Positioned(
                      top: 50,
                      left: 200,
                      child: SizedBox(
                        height: 200,
                        width: 600,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: const [
                            Text(
                              "Frontend Developer",
                              style: TextStyle(
                                  fontSize: 19,
                                  fontFamily: 'Roboto-Thin',
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            ),
                            Divider(
                              color: Colors.black,
                            ),
                            Text(
                              "Mr. Tenzin Kelzang",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontFamily: 'Roboto-Thin',
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              "Student",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontFamily: 'Roboto-Thin',
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              "GCIT",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontFamily: 'Roboto-Thin',
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ))
                ],
              ),
            ),
            SizedBox(
              height: 230,
              child: Stack(
                children: [
                  Positioned(
                      top: 0,
                      left: 10,
                      right: 10,
                      child: Material(
                        child: Container(
                          height: 180,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(0.0),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.3),
                                spreadRadius: 4.0,
                                blurRadius: 20.0,
                                offset: const Offset(
                                    -10.0, 10.0), // changes position of shadow
                              ),
                            ],
                          ),
                        ),
                      )),
                  Positioned(
                      top: 15,
                      left: 30,
                      child: Card(
                        elevation: 10.0,
                        shadowColor: Colors.grey.withOpacity(0.5),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: Container(
                          height: 150,
                          width: 150,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              image: const DecorationImage(
                                fit: BoxFit.fill,
                                image: AssetImage("assets/sonamchoki.jpeg"),
                              )),
                        ),
                      )),
                  Positioned(
                      top: 50,
                      left: 200,
                      child: SizedBox(
                        height: 200,
                        width: 600,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: const [
                            Text(
                              "UI Designer",
                              style: TextStyle(
                                  fontSize: 20,
                                  fontFamily: 'Roboto-Thin',
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            ),
                            Divider(
                              color: Colors.black,
                            ),
                            Text(
                              "Ms. Sonam Choki",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontFamily: 'Roboto-Thin',
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              "Student",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontFamily: 'Roboto-Thin',
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              "GCIT",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontFamily: 'Roboto-Thin',
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
