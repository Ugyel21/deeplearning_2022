import 'dart:io';
import 'package:dzongkha_digits/predicted_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tflite/flutter_tflite.dart';
import 'package:image_picker/image_picker.dart';

class UploadImage extends StatefulWidget {
  const UploadImage({super.key});

  @override
  State<UploadImage> createState() => _UploadImageState();
}

class _UploadImageState extends State<UploadImage> {
  File? _image;
  late List _output;
  final picker = ImagePicker();

  @override
  void initState() {
    super.initState();
    loadModel().then((value) {
      setState(() {});
    });
  }

  @override
  void dispose() {
    super.dispose();
    Tflite.close();
  }

  classifyingImage(image) async {
    var output = await Tflite.runModelOnImage(
      path: image.path,
      numResults: 11,
      threshold: 0.5,
      imageMean: 0.0,
      imageStd: 20.0,
      asynch: true,
    );
    setState(() {
      _output = output!;

      String _name = '${_output[0]['label']}';
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => PredictedScreen(name: _name)));
    });
  }

  loadModel() async {
    await Tflite.loadModel(
        model: 'assets/handwritten.tflite', labels: 'assets/labels.txt');
  }

  pickImage() async {
    var image = await picker.pickImage(source: ImageSource.gallery);
    if (image == null) return null;

    setState(() {
      _image = File(image.path);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 1,
        centerTitle: false,
        title: const Text(
          "Dzongkha Handwritten Digit",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18.0,
            fontFamily: 'Roboto-Thin',
            color: Colors.white,
          ),
        ),
        backgroundColor: const Color.fromARGB(255, 61, 85, 222),
        elevation: 0,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: pickImage,
        backgroundColor: Colors.blue,
        child: const Icon(Icons.image),
      ),
      body: _image == null
          ? Container(
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  CircularProgressIndicator(),
                  SizedBox(
                    height: 50,
                  ),
                  Text(
                    "Please pick images from your gallery",
                    style: TextStyle(
                      fontSize: 18.0,
                      fontFamily: 'Roboto-Thin',
                      color: Colors.black,
                    ),
                  )
                ],
              ),
            )
          : Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: SizedBox(
                    height: 400 + 4 * 2,
                    width: 400 + 4 * 2,
                    child: Image.file(
                      _image!,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 40,
                ),
                Container(
                  alignment: Alignment.center,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 40,
                        width: 150,
                        child: TextButton(
                          style: TextButton.styleFrom(
                              primary: Colors.white,
                              backgroundColor: Colors.blue,
                              onSurface: Colors.grey),
                          onPressed: () {
                            classifyingImage(_image);
                          },
                          child: const Text(
                            "Predict",
                            style: TextStyle(
                              fontSize: 18.0,
                              fontFamily: 'Roboto-Thin',
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
    );
  }
}
