import cv2
import glob
import os

i = 0
for img in glob.glob("*.jpg"):
    image = cv2.imread(img)
    imgResized = cv2.resize(image, (250, 250))
    cv2.imwrite("Resized%04i.jpg" %i, imgResized)
    i += 1
    cv2.imshow("image", imgResized)
    cv2.waitKey(30)
cv2.destroyAllWindows()